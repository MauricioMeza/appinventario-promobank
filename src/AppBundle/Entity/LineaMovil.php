<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entidad para los datos de las tarjetas Sim que van incertadas en los equipos
 *
 * @author Mauricio Meza
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="LineaMovil")
 */
class LineaMovil {
    
    


    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", name="LINE_ID")
     */
    private $id; //auto
    
    /**
     *@ORM\Column(type="string", name="LINE_NUMERO") 
     */
    private $numero;
    
    /**
     *@ORM\Column(type="string", name="LINE_COMPANIA")
     */
    private $compania; 
    
    /**
     *@ORM\Column(type="boolean", name="LINE_HABIL") 
     */
    private $habil; //hábil si/no
    
    /**
     *@ORM\Column(type="string", name="LINE_SIMCARD", nullable=true) 
     */
    private $simcard;//código de serie de la simcard no obligatorio
    
    /**
     *@ORM\Column(type="boolean", name="LINE_OCULTO", options={"default" = false}) 
     */
    private $oculto;//para ocultar campos en las listas
    
    /**
     * @ORM\Column(type="boolean", name="LINE_ASIGNADO", nullable=true, options={"default" = false})
     */
    private $asignado;//para que no figure como opcion de asignación una línea ya asignada

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return LineaMovil
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set compania
     *
     * @param string $compania
     * @return LineaMovil
     */
    public function setCompania($compania)
    {
        $this->compania = $compania;

        return $this;
    }

    /**
     * Get compania
     *
     * @return string 
     */
    public function getCompania()
    {
        return $this->compania;
    }

    /**
     * Set habil
     *
     * @param boolean $habil
     * @return LineaMovil
     */
    public function setHabil($habil)
    {
        $this->habil = $habil;

        return $this;
    }

    /**
     * Get habil
     *
     * @return boolean 
     */
    public function getHabil()
    {
        return $this->habil;
    }

    /**
     * Set simcard
     *
     * @param string $simcard
     * @return LineaMovil
     */
    public function setSimcard($simcard)
    {
        $this->simcard = $simcard;

        return $this;
    }

    /**
     * Get simcard
     *
     * @return string 
     */
    public function getSimcard()
    {
        return $this->simcard;
    }

    /**
     * Set oculto
     *
     * @param boolean $oculto
     * @return LineaMovil
     */
    public function setOculto($oculto)
    {
        $this->oculto = $oculto;

        return $this;
    }

    /**
     * Get oculto
     *
     * @return boolean 
     */
    public function getOculto()
    {
        return $this->oculto;
    }

    /**
     * Set asignado
     *
     * @param boolean $asignado
     * @return LineaMovil
     */
    public function setAsignado($asignado)
    {
        $this->asignado = $asignado;

        return $this;
    }

    /**
     * Get asignado
     *
     * @return boolean 
     */
    public function getAsignado()
    {
        return $this->asignado;
    }
}
