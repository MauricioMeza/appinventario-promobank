<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entidad para los datos de la tabla que almacena los datos de los equipos
 *
 * @author Mauricio Meza
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="Celular")
 */
class Celular {
       
    /**
     * @ORM\OneToOne(targetEntity="LineaMovil")
     * @ORM\JoinColumn(name="CELU_LINE_ID", referencedColumnName="LINE_ID")
     */
    protected $lineaMovil;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", name="CELU_ID")
     */
    private $id;
    
    /**
     *@ORM\Column(type="string", name="CELU_IMEI")
     */
    private $imei;
    
    /**
     *@ORM\Column(type="string", name="CELU_MARCA") 
     */
    private $marca;
    
    /**
     *@ORM\Column(type="string", name="CELU_MODELO") 
     */
    private $modelo;
    
    /**
     *@ORM\Column(type="boolean", name="CELU_HABIL")
     */
    private $habil; //habilitado si/no
    
    /**
     *@ORM\Column(type="text", name="CELU_OBSERVACION", nullable=true) 
     */
    private $observacion; //detalles en el quipo (campo no obligatorio)
    
    /**
     *@ORM\Column(type="boolean", name="CELU_OCULTO", options={"default" = true}) 
     */
    private $oculto;//para ocultar campos en las listas
    
    /**
     * @ORM\Column(type="boolean", name="CELU_ASIGNADO", nullable=true, options={"default" = false})
     */
    private $asignado;//para que no figure como opcion de asignación un celular ya asignado


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imei
     *
     * @param string $imei
     * @return Celular
     */
    public function setImei($imei)
    {
        $this->imei = $imei;

        return $this;
    }

    /**
     * Get imei
     *
     * @return string 
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * Set marca
     *
     * @param string $marca
     * @return Celular
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return string 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     * @return Celular
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set habil
     *
     * @param boolean $habil
     * @return Celular
     */
    public function setHabil($habil)
    {
        $this->habil = $habil;

        return $this;
    }

    /**
     * Get habil
     *
     * @return boolean 
     */
    public function getHabil()
    {
        return $this->habil;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return Celular
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set oculto
     *
     * @param boolean $oculto
     * @return Celular
     */
    public function setOculto($oculto)
    {
        $this->oculto = $oculto;

        return $this;
    }

    /**
     * Get oculto
     *
     * @return boolean 
     */
    public function getOculto()
    {
        return $this->oculto;
    }

    /**
     * Set asignado
     *
     * @param boolean $asignado
     * @return Celular
     */
    public function setAsignado($asignado)
    {
        $this->asignado = $asignado;

        return $this;
    }

    /**
     * Get asignado
     *
     * @return boolean 
     */
    public function getAsignado()
    {
        return $this->asignado;
    }

    /**
     * Set lineaMovil
     *
     * @param \AppBundle\Entity\LineaMovil $lineaMovil
     * @return Celular
     */
    public function setLineaMovil(\AppBundle\Entity\LineaMovil $lineaMovil = null)
    {
        $this->lineaMovil = $lineaMovil;

        return $this;
    }

    /**
     * Get lineaMovil
     *
     * @return \AppBundle\Entity\LineaMovil 
     */
    public function getLineaMovil()
    {
        return $this->lineaMovil;
    }
}
