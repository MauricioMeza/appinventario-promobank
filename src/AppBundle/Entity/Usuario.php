<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entidad para los datos de usuarios que utilizan los equipos
 * 
 * @author Mauricio Meza
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="Usuario")
 */
class Usuario {
    
    /**
     * @ORM\OneToOne(targetEntity="Celular")
     * @ORM\JoinColumn(name="USUA_CELU_ID", referencedColumnName="CELU_ID")
     */
    protected $celular;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", name="USUA_ID")
     */
    private $id; //auto
    
    /**
     * @ORM\Column(type="string", name="USUA_RUT") 
     */
    private $rut;
    
    /**
     * @ORM\COlumn(type="string", name="USUA_NOMBRES") 
     */
    private $nombres;
    
    /**
     * @ORM\Column(type="string", name="USUA_APELLIDOPATERNO") 
     */
    private $apellidoPaterno;
    
    /**
     * @ORM\Column(type="string", name="USUA_APELLIDOMATERNO") 
     */
    private $apellidoMaterno;
    
    /**
     * @ORM\Column(type="string", name="USUA_AREA")
     */
    private $area; //area de trabajo /departamento
    
    /**
     * @ORM\Column(type="string", name="USUA_DIRECCION") 
     */
    private $direccion;
    
    /**
     * @ORM\Column(type="string", name="USUA_CORREOELECTRONICO", nullable=true) 
     */
    private $correoElectronico;
    
    /**
     * @ORM\Column(type="string", name="USUA_NUMERODECONTACTO") 
     */
    private $numeroDeContacto; //distinto al número de sim asociado
    
    /**
     * @ORM\Column(type="boolean", name="USUA_HABIL")
     */
    private $habil; //empleado vigente o no-vigente

    /**
     *@ORM\Column(type="boolean", name="USUA_OCULTO", options={"default" = true}) 
     */
    private $oculto;//para ocultar campos en las listas


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rut
     *
     * @param string $rut
     * @return Usuario
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * Get rut
     *
     * @return string 
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     * @return Usuario
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string 
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellidoPaterno
     *
     * @param string $apellidoPaterno
     * @return Usuario
     */
    public function setApellidoPaterno($apellidoPaterno)
    {
        $this->apellidoPaterno = $apellidoPaterno;

        return $this;
    }

    /**
     * Get apellidoPaterno
     *
     * @return string 
     */
    public function getApellidoPaterno()
    {
        return $this->apellidoPaterno;
    }

    /**
     * Set apellidoMaterno
     *
     * @param string $apellidoMaterno
     * @return Usuario
     */
    public function setApellidoMaterno($apellidoMaterno)
    {
        $this->apellidoMaterno = $apellidoMaterno;

        return $this;
    }

    /**
     * Get apellidoMaterno
     *
     * @return string 
     */
    public function getApellidoMaterno()
    {
        return $this->apellidoMaterno;
    }

    /**
     * Set area
     *
     * @param string $area
     * @return Usuario
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Usuario
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set correoElectronico
     *
     * @param string $correoElectronico
     * @return Usuario
     */
    public function setCorreoElectronico($correoElectronico)
    {
        $this->correoElectronico = $correoElectronico;

        return $this;
    }

    /**
     * Get correoElectronico
     *
     * @return string 
     */
    public function getCorreoElectronico()
    {
        return $this->correoElectronico;
    }

    /**
     * Set numeroDeContacto
     *
     * @param string $numeroDeContacto
     * @return Usuario
     */
    public function setNumeroDeContacto($numeroDeContacto)
    {
        $this->numeroDeContacto = $numeroDeContacto;

        return $this;
    }

    /**
     * Get numeroDeContacto
     *
     * @return string 
     */
    public function getNumeroDeContacto()
    {
        return $this->numeroDeContacto;
    }

    /**
     * Set habil
     *
     * @param boolean $habil
     * @return Usuario
     */
    public function setHabil($habil)
    {
        $this->habil = $habil;

        return $this;
    }

    /**
     * Get habil
     *
     * @return boolean 
     */
    public function getHabil()
    {
        return $this->habil;
    }

    /**
     * Set oculto
     *
     * @param boolean $oculto
     * @return Usuario
     */
    public function setOculto($oculto)
    {
        $this->oculto = $oculto;

        return $this;
    }

    /**
     * Get oculto
     *
     * @return boolean 
     */
    public function getOculto()
    {
        return $this->oculto;
    }

    /**
     * Set celular
     *
     * @param \AppBundle\Entity\Celular $celular
     * @return Usuario
     */
    public function setCelular(\AppBundle\Entity\Celular $celular = null)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return \AppBundle\Entity\Celular 
     */
    public function getCelular()
    {
        return $this->celular;
    }
}
