<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entidad encargada de guardar registros de las operaciones realizadas en la aplicación
 *
 * @author Mauricio Meza
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="Registro")
 */
class Log {
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", name="REGI_ID")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", name="REGI_CATEGORIA") 
     */
    private $categoria;
    
    /**
     * @ORM\Column(type="string", name="REGI_TIPO") 
     */
    private $tipo;
    
    /**
     * @ORM\Column(type="string", name="REGI_FECHA")
     */
    private $fecha;
    
    /**
     * @ORM\Column(type="string", name="REGI_HORA") 
     */   
    private $hora;
    
    /**
     * @ORM\Column(type="text", name="REGI_ACCION")
     */
    private $accion;

    
    // getters & setters


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     * @return Log
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Log
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set fecha
     *
     * @param string $fecha
     * @return Log
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return string 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set hora
     *
     * @param string $hora
     * @return Log
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get hora
     *
     * @return string 
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set accion
     *
     * @param string $accion
     * @return Log
     */
    public function setAccion($accion)
    {
        $this->accion = $accion;

        return $this;
    }

    /**
     * Get accion
     *
     * @return string 
     */
    public function getAccion()
    {
        return $this->accion;
    }
}
