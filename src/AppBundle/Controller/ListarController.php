<?php
namespace AppBundle\Controller; 

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * Controlador que lista las tablas de la base de datos
 *
 * @author Mauricio Meza
 */
class ListarController extends Controller{
    
    /**
     * @Route("/lista.lineas_moviles", name="listar_lineamovil")
     */
    public function listarLineaMovilAction()
    {
        $lineasMoviles = $this->getDoctrine()
                ->getRepository('AppBundle:LineaMovil')
                ->findBy(
                        array('oculto' => false),
                        array('numero' => 'ASC')
                        );
        return $this->render('listar/listarLineasMoviles.html.twig', array('lineasMoviles'=>$lineasMoviles));
    }
    
    /**
     * @Route("lista.celulares", name="listar_celulares")
     */
    public function listarCelularesAction()
    {
        $celulares = $this->getDoctrine()
                ->getRepository('AppBundle:Celular')
                ->findBy(
                        array('oculto' => false),
                        array('imei' => 'ASC')
                        );
        
        
        return $this->render('listar/listarCelulares.html.twig', array('celulares'=>$celulares));
    }
    
    /**
     * @Route("/lista.usuarios", name="listar_usuarios")
     */
    public function listarUsuariosAction()
    {
        $usuarios = $this->getDoctrine()
                ->getRepository('AppBundle:Usuario')
                ->findBy(
                        array('oculto' => false),
                        array('rut' => 'ASC')
                        );
        return $this->render('listar/listarUsuarios.html.twig', array('usuarios'=>$usuarios));
    }
    
    /**
     * @Route("/lista.log", name="listar_log")
     */
    public function listarLogAction()
    {
        $logs = $this->getDoctrine()
                ->getRepository('AppBundle:Log')
                ->findAll();
        return $this->render('listar/listarLog.html.twig', array('logs'=>$logs));
    }
}
