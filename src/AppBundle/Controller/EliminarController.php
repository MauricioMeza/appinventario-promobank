<?php
namespace AppBundle\Controller; 

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Metodos\EliminarLog;

/**
 * Controlador que se encarga de eliminar campos de las tablas de la base de datos
 *
 * @author Mauricio Meza
 */
class EliminarController extends Controller{
    
    /**
     * @Route("/el/{idLinea}", name="eliminar_linea", options={"expose"=true})
     */
    public function eliminarLineaAction($idLinea){
        $log = new EliminarLog();
        $em = $this->getDoctrine()->getManager();
        $linea = $em->getRepository('AppBundle:LineaMovil')->find($idLinea);
        $linea->setOculto(true);
        if($linea->getAsignado() === false){
            $em->persist($linea);
            $em->flush();
            $log->eliminarLineaLog($em, $linea);
            return $this->redirect($this->generateUrl('listar_lineamovil'));}
        else{   
            $celular = $em->getRepository('AppBundle:Celular')->findOneByLineaMovil($idLinea);
            $celular->setLineaMovil(null);
            $primero = $em->persist($celular);
            $em->flush($primero);
            $segundo = $em->persist($linea);
            $em->flush($segundo);
            $log->eliminarLineaLog($em, $linea);
            return $this->redirect($this->generateUrl('listar_lineamovil'));}
    }
    
    /**
     * @Route("/elmc/{idCelular}", name="eliminar_celular", options={"expose"=true})
     */
    public function eliminarCelularAction($idCelular){
        $log = new EliminarLog();
        $em = $this->getDoctrine()->getManager();
        $celular = $em->getRepository('AppBundle:Celular')->find($idCelular);
        $celular->setOculto(true);
        if($celular->getAsignado() === false){
            if($celular->getLineaMovil() !== null){$celular->getLineaMovil()->setAsignado(false);}
            $celular->setLineaMovil(null);
            $em->persist($celular);
            $em->flush();
            $log->eliminarCelularLog($em, $celular);
            return $this->redirect($this->generateUrl('listar_celulares'));}else{
            $usuario = $em->getRepository('AppBundle:Usuario')->findOneByCelular($idCelular);
            $usuario->setCelular(null);
            $primero = $em->persist($usuario);
            $em->flush($primero);
            if($celular->getLineaMovil() !== null){$celular->getLineaMovil()->setAsignado(false);}
            $celular->setLineaMovil(null);
            $segundo = $em->persist($celular);
            $em->flush($segundo);
            $log->eliminarCelularLog($em, $celular);
            return $this->redirect($this->generateUrl('listar_celulares'));}}
    
    /**
     * @Route("/elmu/{idUsuario}", name="eliminar_usuario", options={"expose"=true})
     */
    public function eliminarUsuarioAction($idUsuario)
    {
        $log = new EliminarLog();
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->find($idUsuario);
        $usuario->setOculto(true);
        if($usuario->getCelular() !== null){
            $usuario->getCelular()->setAsignado(false);
        }
        $usuario->setCelular(null);
        $em->flush();
        $log->eliminarUsuarioLog($em, $usuario);
        return $this->redirect($this->generateUrl('listar_usuarios'));
    }
}
