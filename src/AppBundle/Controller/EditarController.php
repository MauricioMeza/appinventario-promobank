<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\Type\LineaMovilFormType;
use AppBundle\Form\Type\EditCelularFormType;
use AppBundle\Form\Type\EditUsuarioFormType;
use AppBundle\Form\Type\EditCelularAsignacionFormType;
use AppBundle\Form\Type\UsuarioFormType;

use AppBundle\Metodos\EditarLog;

/**
 * Controlador encargado de realizar las acciones para editar los campos de las tablas de la aplicación
 *
 * @author Mauricio Meza
 */
class EditarController extends Controller {
    
    /**
     * @Route("/editar.linea_movil/{id}", name="editar_linea")
     */
    public function enviarEditarLineaAction(Request $request, $id){
        $log = new EditarLog();//se llama al método que inserta el log
        $em = $this->getDoctrine()->getEntityManager();
        $lineaMovil = $em->getRepository('AppBundle:LineaMovil')->find($id);
        $form = $this->createForm(new LineaMovilFormType(), $lineaMovil);
        $form->handleRequest($request);
        if ($form->isValid()){
                $em->persist($lineaMovil);
                $em->flush(); 
                $log->editarLineaLog($em);//se registra el log
                return $this->redirect($this->generateUrl('listar_lineamovil'));
        }
        return $this->render('editar/editarLineaMovil.html.twig', array('form' => $form->createView(), 'id' =>$lineaMovil));
    }
    
    /**
     * @Route("/editar.celular/{id}", name="editar_celular")
     */
    public function editarCelularAction(Request $request, $id){
        $log = new EditarLog();//se llama a la clase que inserta el log
        $em = $this->getDoctrine()->getEntityManager();
        $celular = $em->getRepository('AppBundle:Celular')->find($id);//se busca el celular a editar
        $lineasMoviles = $this->getDoctrine()->getRepository('AppBundle:LineaMovil')->findBy(array('asignado' => false,'oculto' => false));
        if($celular->getLineaMovil() !== null || empty($lineasMoviles)){//si la línea móvil no es null o si es que no quedan líneas móviles
            $form = $this->createForm(new EditCelularAsignacionFormType(), $celular);//se llama al formulario que no posee asignación de línea
            if($celular->getLineaMovil() === null){$asignacion = false;}//en el caso que no queden líneas asignación será false
            else{$asignacion = true;}//si quedan líneas y además el celular tiene asignación esta variable es true
            $form->handleRequest($request);//se le entregan los campos sin editar al formulario
            if ($form->isValid()){//si el formulario está correcto
                $em->persist($celular);
                $em->flush();
                $log->editarCelularLog($em);//se registra el log
                return $this->redirect($this->generateUrl('listar_celulares'));
            }//cuando la edición se complete se redireccionará a la lista de celulares
            return $this->render('editar/editarCelularAsignacion.html.twig',array('form'=>$form->createView(),'id'=>$celular,'asignacion'=>$asignacion));
        }else{//si el celular no tiene asignación y aún hay lineas disponibles se envía a un formulario que obliga a asignar una
            $form = $this->createForm(new EditCelularFormType(), $celular);//se llama al formulario que posee asignación de línea
            $form->handleRequest($request);
            if ($form->isValid()){           
                $celular->getLineaMovil()->setAsignado(true);//el campo "asignado" de la línea pasa a true
                $em->persist($celular);$em->flush();$log->editarCelularLog($em);
                return $this->redirect($this->generateUrl('listar_celulares'));}
            return $this->render('editar/editarCelular.html.twig', array('form' => $form->createView(), 'id' =>$celular));
        }
    }
    
    /**
     * @Route("/editar.usuario/{id}", name="editar_usuario")
     */
    public function editarUsuarioAction(Request $request, $id){
        $log = new EditarLog();//se llama a la clase que inserta el log
        $em = $this->getDoctrine()->getEntityManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->find($id);
        $celulares = $this->getDoctrine()->getRepository('AppBundle:Celular')->findBy(array('asignado' => false,'oculto' => false));
        if($usuario->getCelular() !== null || empty($celulares)){$form = $this->createForm(new UsuarioFormType(), $usuario);
            if($usuario->getCelular() === null){$asignacion = false;}
            else{$asignacion = true;}
            $form->handleRequest($request);
            if ($form->isValid()){$em->persist($usuario);
                $em->flush();
                $log->editarUsuarioLog($em);
                return $this->redirect($this->generateUrl('listar_usuarios'));}
            return $this->render('editar/editarUsuarioAsignacion.html.twig', array('form' => $form->createView(), 'id' =>$usuario, 'asignacion' => $asignacion));}
        else{$form = $this->createForm(new EditUsuarioFormType(), $usuario);
            $form->handleRequest($request); if ($form->isValid()){
                $usuario->getCelular()->setAsignado(true);
                $em->persist($usuario);
                $em->flush();
                $log->editarUsuarioLog($em);
                return $this->redirect($this->generateUrl('listar_usuarios'));}
            return $this->render('editar/editarUsuario.html.twig', array('form' => $form->createView(), 'id' =>$usuario));}}
}
