<?php
 
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Controlador para desasignar líneas móviles de celulares y celulares de usuarios
 *
 * @author Mauricio Meza
 */
class DesasignarController extends Controller {
    
    /**
     * @Route("/dldc/{id}", name="desasignar_linea", options={"expose"=true})
     */
    public function desasignarLineaDeCelularAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $celular = $em->getRepository('AppBundle:Celular')->find($id);
        if (!$celular) {
            throw $this->createNotFoundException(
                'Ha ocurrido un error al intentar desasignar la línea móvil del celular id: '.$id
            );
        }
        $celular->getLineaMovil()->setAsignado(false);
        $celular->setLineaMovil(null);
        $em->persist($celular);
        $em->flush();
        return $this->redirect($this->generateUrl('listar_celulares'));
    }

    /**
     * @Route("/dcdu/{id}", name="desasignar_celular", options={"expose"=true})
     */
    public function desasignarCelularDeUsuarioAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->find($id);
        if (!$usuario) {
            throw $this->createNotFoundException(
                'Ha ocurrido un error al intentar desasignar la línea móvil del celular id: '.$id
            );
        }
        $usuario->getCelular()->setAsignado(false);
        $usuario->setCelular(null);
        $em->persist($usuario);
        $em->flush();
        return $this->redirect($this->generateUrl('listar_usuarios'));
    }
    
    /**
     * @Route("/dldu/{id}", name="desasignar_linea_usuario", options={"expose"=true})
     */
    public function desasignarLineaDeUsuarioAction($id) 
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->find($id);
        if (!$usuario) {
            throw $this->createNotFoundException(
                'Ha ocurrido un error al intentar desasignar la línea móvil del celular id: '.$id
            );
        }
        $usuario->getCelular()->getLineaMovil()->setAsignado(false);
        $usuario->getCelular()->setLineaMovil(null);
        $em->persist($usuario);
        $em->flush();
        return $this->redirect($this->generateUrl('listar_usuarios'));
    }
}
