<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Celular;
use AppBundle\Entity\LineaMovil;
use AppBundle\Entity\Usuario;
use AppBundle\Metodos\AgregarLog;
use AppBundle\Form\Type\LineaMovilFormType;
use AppBundle\Form\Type\CelularFormType;
use AppBundle\Form\Type\UsuarioFormType;

/**
 * Controlador encargado de gestionar las acciones de administrador como mantenedor de datos 
 *
 * @author Mauricio Meza
 */
class AdminController extends Controller{
    
    /**
     * @Route("/agregar.linea_movil", name="agregar_sim", defaults={"agregado" = false})
     */
    public function agregarSimAction(Request $request, $agregado){
        $lineaMovil = new LineaMovil();
        $log = new AgregarLog();
        $form = $this->createForm(new LineaMovilFormType(), $lineaMovil);        
        $form->handleRequest($request);
        if ($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($lineaMovil);
            $em->flush();
            $log->agregarLineaLog($em, $lineaMovil);
            $agregado = true;   
            $lineaMovil = new LineaMovil();
            $form = $this->createForm(new LineaMovilFormType(), $lineaMovil);
            return $this->render('admin/agregarSim.html.twig', array('form' => $form->createView(),'agregado' => $agregado));
        }        
        return $this->render('admin/agregarSim.html.twig', array('form' => $form->createView(),'agregado' => $agregado));
    }

    /**
    * @Route("/agregar.celular", name="agregar_celular", defaults={"agregado" = false})
    */
    public function agregarCelularAction(Request $request, $agregado){
        $celular = new Celular();
        $log = new AgregarLog();
        $form = $this->createForm(new CelularFormType(),$celular);       
        $form->handleRequest($request);
        if ($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($celular);
            $em->flush();
            $log->agregarCelularLog($em, $celular);
            $agregado=true; 
            $celular = new Celular();
            $form = $this->createForm(new CelularFormType(),$celular);
            return $this->render('admin/agregarCel.html.twig', array('form' => $form->createView(),'agregado' => $agregado ));   
        }
        return $this->render('admin/agregarCel.html.twig', array('form' => $form->createView(),'agregado' => $agregado ));   
    }
    
    /**
     * @Route("/agregar.usuario", name="agregar_usuario", defaults={"agregado" = false})
     */
    public function agregarUsuarioAction(Request $request, $agregado){
        $usuario = new Usuario();
        $log = new AgregarLog();
        $form = $this->createForm(new UsuarioFormType(),$usuario);
        $form->handleRequest($request);
        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            $log->agregarUsuarioLog($em, $usuario);
            $agregado=true;
            $usuario = new Usuario();
            $form = $this->createForm(new UsuarioFormType(),$usuario);
            return $this->render('admin/agregarUsuario.html.twig', array('form' => $form->createView(),'agregado' => $agregado));
        }        
        return $this->render('admin/agregarUsuario.html.twig', array('form' => $form->createView(),'agregado' => $agregado));
    }
}
