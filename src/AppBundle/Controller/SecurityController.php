<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Log; 

/**
 * Controlador de la seguridad de la aplicacion
 *
 * @author Mauricio Meza
 */
class SecurityController extends Controller{
    
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'security/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );
    }
    
    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(){
        $this->container->get('security.context')->setToken(null);
        
        $log = new Log();
        $em = $this->getDoctrine()->getManager();
        $log->setCategoria('SESION');
        $log->setTipo('LOGOUT');
        $log->setFecha(strftime('%Y-%m-%d'));
        $log->setHora(strftime('%H:%M'));
        $log->setAccion('Se ha finalizado la sesión');
        $em->persist($log);
        $em->flush();
        
        return $this->redirect($this->generateUrl('homepage'));
    }
}
