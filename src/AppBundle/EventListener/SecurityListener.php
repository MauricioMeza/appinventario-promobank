<?php
namespace AppBundle\EventListener;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

use AppBundle\Entity\Log;

/**
 * Description of RegistrarLog
 *
 * @author Mauricio Meza
 */
class SecurityListener{

    /** @var \Symfony\Component\Security\Core\SecurityContext */
    private $securityContext;
    
    /** @var \Doctrine\ORM\EntityManager */
    private $em;
    
    /**
	 * Constructor
	 * 
	 * @param SecurityContext $securityContext
	 * @param Doctrine        $doctrine
	 */
    public function __construct(SecurityContext $securityContext, Doctrine $doctrine) 
    {
        $this->securityContext = $securityContext;
        $this->em = $doctrine->getEntityManager();
    }
    
    /**
     * Do the magic.
     * 
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin()
    {
        if ($this->securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            $log = new Log();
            $log->setCategoria('SESION');
            $log->setTipo('LOGIN');
            $log->setFecha(strftime('%Y-%m-%d'));
            $log->setHora(strftime('%H:%M'));
            $log->setAccion('Se ha iniciado sesión');
            $this->em->persist($log);
            $this->em->flush();     
        }
    }
}
