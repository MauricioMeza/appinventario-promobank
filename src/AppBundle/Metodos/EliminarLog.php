<?php
namespace AppBundle\Metodos;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Log;

use AppBundle\Entity\LineaMovil;
use AppBundle\Entity\Celular;
use AppBundle\Entity\Usuario;

class EliminarLog {
    public function eliminarLineaLog(EntityManager $em,LineaMovil $linea){
        $log = new Log();
        $log->setCategoria('LINEA_MOVIL');
        $log->setTipo('ELIMINADO');
        $log->setAccion('Línea Móvil '.$linea->getNumero().' eliminada');
        $log->setFecha(strftime('%Y-%m-%d'));
        $log->setHora(strftime('%H:%M'));
        $em->persist($log);
        $em->flush();
        return $this;
    }
    public function eliminarCelularLog(EntityManager $em,Celular $celular){
        $log = new Log();
        $log->setCategoria('CELULAR');
        $log->setTipo('ELIMINADO');
        $log->setAccion('Celular '.$celular->getImei().' Equipo '.$celular->getMarca().' '.$celular->getModelo().' eliminado');
        $log->setFecha(strftime('%Y-%m-%d'));
        $log->setHora(strftime('%H:%M'));
        $em->persist($log);
        $em->flush();
        return $this;        
    }
    public function eliminarUsuarioLog(EntityManager $em,Usuario $usuario) {
        $log = new Log();
        $log->setCategoria('USUARIO');
        $log->setTipo('ELIMINADO');
        $log->setAccion('Usuario '.$usuario->getNombres().' '.$usuario->getApellidoPaterno().
                ' '.$usuario->getApellidoMaterno().' RUT: '.$usuario->getRut().' eliminado(a)');
        $log->setFecha(strftime('%Y-%m-%d'));
        $log->setHora(strftime('%H:%M'));
        $em->persist($log);
        $em->flush();
        return $this;
    }
}
