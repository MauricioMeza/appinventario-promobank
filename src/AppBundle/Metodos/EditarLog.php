<?php
namespace AppBundle\Metodos;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Log;

class EditarLog {
    public function editarLineaLog(EntityManager $em) {
        $log = new Log();
        $log->setCategoria('LINEA_MOVIL');
        $log->setTipo('EDITADO');
        $log->setAccion('línea editada');
        $log->setFecha(strftime('%Y-%m-%d'));
        $log->setHora(strftime('%H:%M'));
        $em->persist($log);
        $em->flush(); 
        return $this;
    }
    public function editarCelularLog(EntityManager $em) {
        $log = new Log();
        $log->setCategoria('CELULAR');
        $log->setTipo('EDITADO');
        $log->setAccion('celular editado');
        $log->setFecha(strftime('%Y-%m-%d'));
        $log->setHora(strftime('%H:%M'));
        $em->persist($log);
        $em->flush(); 
        return $this;
    }
    public function editarUsuarioLog(EntityManager $em) {
        $log = new Log();
        $log->setCategoria('USUARIO');
        $log->setTipo('EDITADO');
        $log->setAccion('usuario editado');
        $log->setFecha(strftime('%Y-%m-%d'));
        $log->setHora(strftime('%H:%M'));
        $em->persist($log);
        $em->flush(); 
        return $this;
    }
}
