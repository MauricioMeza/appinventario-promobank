<?php
namespace AppBundle\Metodos;
use AppBundle\Entity\Log;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\LineaMovil;
use AppBundle\Entity\Celular;
use AppBundle\Entity\Usuario;

class AgregarLog {
    public function agregarLineaLog(EntityManager $em,LineaMovil $lineaMovil) {
        $log = new Log();
        $log->setCategoria('LINEA_MOVIL');
        $log->setTipo('AGREGADO');
        $log->setAccion('Línea Móvil '.$lineaMovil->getNumero().' agregada');
        $log->setFecha(strftime('%Y-%m-%d'));
        $log->setHora(strftime('%H:%M'));
        $em->persist($log);
        $em->flush();
        return $this;
    }
    public function agregarCelularLog(EntityManager $em,Celular $celular){
        $log = new Log();
        $log->setCategoria('CELULAR');
        $log->setTipo('AGREGADO');
        $log->setAccion('Celular '.$celular->getImei().' equipo '.$celular->getMarca().' '.$celular->getModelo().' agregado');
        $log->setFecha(strftime('%Y-%m-%d'));
        $log->setHora(strftime('%H:%M'));
        $em->persist($log);
        $em->flush();
        return $this;
    }
    public function agregarUsuarioLog(EntityManager $em,Usuario $usuario) {
        $log = new Log();
        $log->setCategoria('USUARIO');
        $log->setTipo('AGREGADO');
        $log->setAccion('Usuario '.$usuario->getNombres().' '.$usuario->getApellidoPaterno().' '.$usuario->getApellidoMaterno().' RUT: '.$usuario->getRut().' agregado(a)');
        $log->setFecha(strftime('%Y-%m-%d'));
        $log->setHora(strftime('%H:%M'));
        $em->persist($log);
        $em->flush();
        return $this;
    }
}
