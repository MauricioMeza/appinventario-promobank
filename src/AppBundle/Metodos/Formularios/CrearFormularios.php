<?php
namespace AppBundle\Metodos\Formularios;
use Symfony\Component\Form\FormBuilderInterface;

class CrearFormularios {
    public function usuarioForm(FormBuilderInterface $builder) {
        $builder
                ->add('rut', 'text')
                ->add('nombres', 'text')
                ->add('apellidoPaterno','text')
                ->add('apellidoMaterno','text')
                ->add('area','text')
                ->add('direccion','text')
                ->add('correoElectronico','email', array('required' => FALSE))
                ->add('numeroDeContacto','text')
                ->add('habil','choice', array(
                    'choices'=>array('Empleado habilitado'=>TRUE,'Empleado inhabilitado'=>false),
                    'choices_as_values' => true))
                ->add('oculto', 'hidden', array('label' => 'Field','data' => '0'));
        return $this;
    }
}
