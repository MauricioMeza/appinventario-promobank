<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Clase que genera el formulario para editar celulares agregan el campo para estableser la asignación de líneas móviles
 *
 * @author Mauricio Meza
 */
class EditCelularFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imei','text')
            ->add('marca','text')
            ->add('modelo','text')
            ->add('habil', 'choice', array(
                'choices' => array('Habilitado' => true,'Inhabilitado' => false),'choices_as_values' => true))   
            ->add('observacion','textarea', array('required' => FALSE))
            ->add('oculto', 'hidden', array('label' => 'Field','data' => '0'))
            ->add('lineaMovil', 'entity', array('class' => 'AppBundle:LineaMovil','choice_label' => 'numero',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')->where('u.asignado = :aux', 'u.oculto = :aux2')
                        ->setParameter('aux', false)->setParameter('aux2', false)
                            ->orderBy('u.numero', 'ASC');},));
    }
    public function getName()
    {return 'EditCelular';}
}

