<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Clase que genera el formulario para editar celulares agregan el campo para estableser la asignación de líneas móviles
 *
 * @author Mauricio Meza
 */
class EditUsuarioFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rut', 'text')
            ->add('nombres', 'text')
            ->add('apellidoPaterno','text')
            ->add('apellidoMaterno','text')
            ->add('area','text')
            ->add('direccion','text')
            ->add('correoElectronico','email', array('required' => FALSE))
            ->add('numeroDeContacto','text')
            ->add('habil','choice', array('choices'=>array(
                'Empleado habilitado'=>TRUE,'Empleado inhabilitado'=>false),'choices_as_values' => true))
            ->add('oculto', 'hidden', array('label' => 'Field','data' => '0'))
            ->add('celular', 'entity', array(
            'class' => 'AppBundle:Celular',
            'choice_label' => 'imei',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('u')
                        ->where('u.asignado = :aux','u.oculto = :aux2')
                        ->setParameter('aux', false)->setParameter('aux2', false)->orderBy('u.imei', 'ASC');},));
    }
    public function getName()
    {return 'EditUsuario';}
}
