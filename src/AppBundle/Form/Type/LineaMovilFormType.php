<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Clase que genera el formulario para el registro de lineas moviles
 *
 * @author Mauricio Meza
 */
class LineaMovilFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('numero', 'text')
                ->add('compania','choice', array('choices' => array('Movistar' => 'Movistar'),'choices_as_values' => true))
                ->add('habil', 'choice', array('choices' => array(
                    'Línea Habilitada' => true,'Línea Inhabilitada' => false),'choices_as_values' => true))
                ->add('simcard', 'text', array('required' => FALSE))
                ->add('oculto', 'hidden', array('label' => 'Field','data' => '0'))
                ->add('asignado', 'hidden', array('label' => 'Field','data' => '0'));
    }
    public function getName()
    {return 'LineaMovil';}
}
