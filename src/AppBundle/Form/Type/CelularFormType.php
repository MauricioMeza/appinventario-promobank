<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Clase que genera el formulario para el registro de celulares
 *
 * @author Mauricio Meza
 */
class CelularFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('imei','text')
                ->add('marca','text')
                ->add('modelo','text')
                ->add('habil', 'choice', array('choices' => array(
                        'Habilitado' => true,'Inhabilitado' => false),'choices_as_values' => true))   
                ->add('observacion','textarea', array('required' => FALSE))
                ->add('oculto', 'hidden', array('label' => 'Field','data' => '0'))
                ->add('asignado', 'hidden', array('label' => 'Field','data' => '0'));
    }
    public function getName()
    {return 'Celular';}
}
