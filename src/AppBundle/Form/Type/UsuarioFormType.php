<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use AppBundle\Metodos\Formularios\CrearFormularios;

/**
 * Clase que genera el formulario para el registro de usuarios
 *
 * @author Mauricio Meza
 */
class UsuarioFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $cf = new CrearFormularios();
        $cf->usuarioForm($builder);
    }
    public function getName()
    {
        return 'Usuario';
    }
}
