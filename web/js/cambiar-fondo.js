$( function(){
    var arrImagenes = [ 'fondo1.jpg','fondo2.jpg', 'fondo3.jpg', 'fondo4.jpg', 'fondo5.jpg', 'fondo6.jpg', 'fondo7.jpg', 'fondo8.jpg', 'fondo9.jpg', 'fondo10.jpg' ];
    var imagenActual = 'fondo1.jpg';
    var tiempo = 12000;
    var id_contenedor = 'main-wrap';
    setInterval( function(){
        do{
            var randImage = arrImagenes[Math.ceil(Math.random()*(arrImagenes.length-1))];
        }while( randImage === imagenActual )
        imagenActual = randImage;
        cambiarImagenFondo(imagenActual, id_contenedor);
    }, tiempo);
});
 
function cambiarImagenFondo(nuevaImagen, estilos){
    var estilos = $('#' + estilos);
    var tempImagen = new Image();
    $(tempImagen).load( function(){
        estilos.css('backgroundImage', 'url('+tempImagen.src+')');
    });
    tempImagen.src = '../img/diseño/' + nuevaImagen;
}